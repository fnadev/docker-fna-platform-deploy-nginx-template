### Installing FNA using the docker-compose file ###

It assumes that you have installed docker and docker-compose on your server.

### Installation with nginx ###

#### File system preparation ####
1. Mount separate volume to “/vol/fna” on your server;
2. Create directories in “/vol/fna”:
    - nginx
    - nginx/ssl
    - fna-data
    - jdbc (if you need connections to external databases)
3. Copy your valid license file fna-license.lic to “/vol/fna/fna-data” 
4. Copy the “nginx.conf” file from the repository to “/vol/fna/nginx”
5. Copy your ssl certificates to “/vol/fna/nginx/ssl” 
6. Copy necessary JDBC drivers to “/vol/fna/jdbc”

#### Configuration ####

Edit the properties you need in the docker-compose.yml file

```java
version: '3.3'
services:
  nginx:
    container_name: proxy
    image: nginx
    restart: always
    ports:
     - 80:80
     - 443:443
    volumes:
     - /vol/fna/nginx:/etc/nginx
  fna-web:
    image: fnarepo/fna-web:$FNA_WEB_VERSION
    expose:
      - "8080"
    environment:
      JAVA_OPTIONS: >-
        -Xms4G
        -Xmx6G
        -XX:MaxPermSize=256m
      FNA_OPTIONS: >-
        -Dserver.jsp-servlet.init-parameters.mappedfile=false
        -Dserver.tomcat.protocolHeader=X-Forwarded-Proto
        -Dserver.tomcat.remoteIpHeader=X-Forwarded-For
        -Dfna.upload.maxSize=16777216000
        -Dfna.core.engine=FNA
        -Dcontext.checker.timer=30000
        -Dcontext.not.running.time=3600000
        -Dstatic.fnacore.files.base.dir=/vol/fna/fna-data
        -Djdbc.url=jdbc:h2:file:/vol/fna/fna-data/fna;MVCC=TRUE
        -Dfna.url=
        -Dfna.root.email=
        -Dmail.host=
        -Dmail.port=
        -Dmail.username=
        -Dmail.password=
        -Dmailchimp.lists.interested.id=
        -Dmailchimp.lists.promotions.id=
        -Dmailchimp.lists.subscribers.id=
        -Dmailchimp.lists.users.id=
        -Dmandrill.apikey=
        -Dldap.domain=
        -Dldap.url=
        -Dldap.group=
    volumes:
      - /vol/fna/fna-data:/vol/fna/fna-data
      - /vol/fna/jdbc:/vol/fna/jdbc

```
 
$ FNA_WEB_VERSION - replace with the version of your choice. List of available versions can be found here: https://cloud.docker.com/u/fnarepo/repository/docker/fnarepo/fna-web

 
#### Docker container start ####
 
Browse to the folder containing docker-compose.yml and run the commands:
 
~~~
docker-compose pull
docker-compose up -d
~~~
 
#### Docker container stop ####
  
Run the command:

~~~
docker-compose down
~~~
 